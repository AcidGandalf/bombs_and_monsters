#include "engine/easy.h"
#include <fstream>
#include <cmath>

using namespace arctic;  // NOLINT

// Prototypes of functions
bool InitGraphics();
void OnKeyDown(Ui32 wParam);
void PaintScreen();
void InitGame();
void LoadLevel(int levelNum);
void LoadLevelFile(int levelNum);
void CBomTim();
void TvarusesMove();
void BombExplode(int x,int y);
void Bomb2Explode(int x,int y);
void Bomb3Explode(int x,int y);
void BlitPanell();
void OnLButtonDown(int x, int y);
void OnLButtonUp(int x, int y);
void OnMouseMove(int x, int y);
void AffectMouse();
void Pause();
void UnPause();
void OnTimer();
void DisplayNumber(char* s, Ui32 x, Ui32 y);
void NewGame();
void Restart();
void WallRedraw();
void CheckOnDone();
void GameComplete();
void CoolStuff();
void Cool();
void UnCool();
void PaintCool();
void SuperTimer();
void CngScr(char cr, int x);
bool InitSound();
void PlayBoomSnd();

// Global variables
bool ddok;
bool setupOK = false;

Sound clickSnd;
Sound unclickSnd;
Sound bounceSnd;
Sound tuhSnd;

Sound boom1Snd;
Sound boom2Snd;
Sound boom3Snd;

Sound death1Snd;

// timers
int tick=1;
int mantick=0;

// Buttons
bool tvarusPressed = false;
bool pausePressed = false;
bool restartPressed = false;
bool exitPressed = false;
bool newPressed = false;
bool coolOnline = false;
bool gameOver = false;
bool restart = false;

// Mouse
Ui32 mousex=0;
Ui32 mousey=0;
Ui32 oldMousex=0;
Ui32 oldMousey=0;
bool lButtonDown = false;
Ui32 mouseCader=0;
Ui32 mouseDirection=0;

// Pause
bool pauseMode = false;
bool exitAfterKey = false;
bool oblomis = false;

// game
int score=100;
int lifes=3;
int levelNum=1;
int bomb2num=0;
int bomb3num=0;
int curputbomb3=0;

// level
const char CLEAR='.',WALL='*',BOMB='B',BOOM='X',TVAR1='T',WALL2='#',
MAN='M',BOMB2='x',WALL3='$',WALL4='A',TVAR2='2',P1LIFE='L',
P5BOM2='5',P1BOM2='1',TVAR3='3',TVAR4='W',TRANS2='R',TVAR5='C',
WALL5='I',TVAR6='U',TRANS5='r',BOMB3='X',P5BOM3='D',P1BOM3='d';

char level[15][14];
int wallredraw[15][14];

char prizes[15][14];

char bombs[15][14];
int bomtim[15][14];

char explode[15][14];
int exptim[15][14];

bool expbomb3 = false;

char creatures[15][14];
char creatures2[15][14];
char creatplus[15][14];
int transtim[15][14];

int manx=1;
int many=1;

bool t1SholdMove = true;

Sprite panellSur;
Sprite coolSur;

bool doExit = false;

void LimitRate(double frame_start) {
  while (true) {
    float dur = 1.f/121.f - (float)(Time() - frame_start);
    if (dur > 0.f) {
      Sleep(dur);
    } else {
      break;
    }
  }
}

void BlitImage(Vec2Si32 pos, Sprite &sprite, Vec4Si32 rect) {
  Si32 left = rect.x;
  Si32 top = rect.y;
  Si32 right = rect.z;
  Si32 bottom = rect.w;
  Si32 wid = right - left;
  Si32 hei = bottom - top;
  
  Si32 sh = ScreenSize().y - 1;
  sprite.Draw(pos.x, sh - pos.y - hei,
              wid, hei,
              left, sprite.Height() - bottom, wid, hei);
  //sprite.Draw(pos.x, pos.y, wid, hei, left, bottom, wid, hei);
}

const double kUpdatePeriod = 0.1;

// Buttons
bool levelPressed=false;

int labx=0;
int laby=0;

char obj=CLEAR;
bool rButtonDown=false;

bool isLevelEditorUnlocked = false;

void UnlockLevelEditor() {
  Ui8 data[1];
  data[0] = 1;
  WriteFile("data/leveleditor.dat", data, 1);
  isLevelEditorUnlocked = true;
}

void EditorBlitPanell() {
  char s[10];
  // TBAPYC
  if(!tvarusPressed)
    BlitImage(Vec2Si32(491, 0),
                        panellSur, Vec4Si32(491, 0, 640, 49));
  else
  {
    BlitImage(Vec2Si32(492, 1),
                        panellSur, Vec4Si32(491, 0, 628, 43));
    BlitImage(Vec2Si32(491, 43),
                        panellSur, Vec4Si32(491, 43, 640, 49));
  }
  // TBAPYC
  //BlitImage(Vec2Si32(491, 0),
  //  panellSur, Vec4Si32(491, 0, 640, 49));
  
  // LEVEL
  BlitImage(Vec2Si32(501,120),
                      panellSur, Vec4Si32(499, 312, 561, 324));
  // level
  sprintf(s, "%d", levelNum);
  DisplayNumber(s, 570, 120);
  
  // EXIT BUTTON
  if(exitPressed)
    BlitImage(Vec2Si32(500, 432),
                        panellSur, Vec4Si32(565, 457, 630, 480));
  else
    BlitImage(Vec2Si32(500, 432),
                        panellSur, Vec4Si32(565, 433, 630, 456));
  
  // NEW BUTTON
  if(newPressed)
    BlitImage(Vec2Si32(500, 378),
                        panellSur, Vec4Si32(495, 349, 561, 372));
  else
    BlitImage(Vec2Si32(500, 378),
                        panellSur, Vec4Si32(495, 325, 561, 348));
  
  // LEVEL BUTTON
  if(!levelPressed)
    BlitImage(Vec2Si32(500, 405),
                        panellSur, Vec4Si32(495, 264, 561, 287));
  else
    BlitImage(Vec2Si32(500, 405),
                        panellSur, Vec4Si32(495, 288, 561, 311));
  
  int x;
  int y;
  x=16;
  y=10;
  
  BlitImage(Vec2Si32(500, 150),
                      panellSur, Vec4Si32(0, 120, 40, 160));
  
  BlitImage(Vec2Si32(540, 150),
                      panellSur, Vec4Si32(40, 120, 80, 160));
  
  BlitImage(Vec2Si32(580, 150),
                      panellSur, Vec4Si32(80, 120, 120, 160));
  
  BlitImage(Vec2Si32(500, 190),
                      panellSur, Vec4Si32(120, 120,
                                        160, 160));
  
  BlitImage(Vec2Si32(540, 190),
                      panellSur, Vec4Si32(240, 120, 280, 160));
  
  BlitImage(Vec2Si32(580, 190),
                      panellSur, Vec4Si32(mantick*40, 160, 40+mantick*40, 200));
  
  BlitImage(Vec2Si32(500, 230),
                      panellSur, Vec4Si32(mantick*40, 200, 40+mantick*40, 240));
  
  BlitImage(Vec2Si32(540, 230),
                      panellSur, Vec4Si32(mantick*40, 240, 40+mantick*40, 280));
  
  BlitImage(Vec2Si32(580, 230),
                      panellSur, Vec4Si32(mantick*40, 360, 40+mantick*40, 400));
  
  BlitImage(Vec2Si32(500, 270),
                      panellSur, Vec4Si32(120+wallredraw[2][2]*40, 120,
                                        160+wallredraw[2][2]*40, 160));
  
  BlitImage(Vec2Si32(540, 270),
                      panellSur, Vec4Si32(mantick*40, 440, 40+mantick*40, 480));
  
  BlitImage(Vec2Si32(580, 270),
                      panellSur, Vec4Si32(160+mantick*40, 160, 200+mantick*40, 200));
  
  BlitImage(Vec2Si32(500, 310),
                      panellSur, Vec4Si32(mantick*40, 280, 40+mantick*40, 320));
  
  BlitImage(Vec2Si32(540, 310),
                      panellSur, Vec4Si32(mantick*40, 320, 40+mantick*40, 360));
  BlitImage(Vec2Si32(565, 315),
                      panellSur, Vec4Si32(631,355,640,368));
  
  BlitImage(Vec2Si32(580, 310),
                      panellSur, Vec4Si32(mantick*40, 320, 40+mantick*40, 360));
  BlitImage(Vec2Si32(605, 315),
                      panellSur, Vec4Si32(631,411,640,424));
  
  BlitImage(Vec2Si32(580, 350),
                      panellSur, Vec4Si32(160+mantick*40, 280,
                                        200+mantick*40, 320));
  BlitImage(Vec2Si32(605, 355),
                      panellSur, Vec4Si32(631,355,640,368));
  
  BlitImage(Vec2Si32(580, 390),
                      panellSur, Vec4Si32(160+mantick*40, 280,
                                        200+mantick*40, 320));
  BlitImage(Vec2Si32(605, 395),
                      panellSur, Vec4Si32(631,411,640,424));
  
  // CURSOR CALCULATIONS
  int xchg=oldMousex-mousex;
  int ychg=oldMousey-mousey;
  
  if((xchg==0)&&(ychg>0))
    mouseDirection=0;
  else if((xchg==0)&&(ychg<0))
    mouseDirection=1;
  else if((xchg>0)&&(ychg==0))
    mouseDirection=2;
  else if((xchg<0)&&(ychg==0))
    mouseDirection=3;
  else if((xchg>0)&&(ychg>0))
    mouseDirection=4;
  else if((xchg<0)&&(ychg>0))
    mouseDirection=5;
  else if((xchg>0)&&(ychg<0))
    mouseDirection=6;
  else if((xchg<0)&&(ychg<0))
    mouseDirection=7;
  
  Ui32 lux=562+(23*mouseCader);
  Ui32 rdx=lux+22;
  Ui32 luy=189+(23*mouseDirection);
  Ui32 rdy=luy+22;
  
  // BLIT MOUSE CURSOR
  BlitImage(Vec2Si32(mousex-11, mousey-11),
                      panellSur, Vec4Si32(lux, luy, rdx, rdy));
}

void EditorPaintScreen() {
  for (int y=0; y < 14; y++)
    for (int x=0; x < 15; x++)
    {
      if(level[x][y]==WALL)
        BlitImage(Vec2Si32(x*32, y*32),
                            panellSur, Vec4Si32(0, 120, 40, 160));
      else if(level[x][y]==WALL2)
        BlitImage(Vec2Si32(x*32, y*32),
                            panellSur, Vec4Si32(40, 120, 80, 160));
      else if(level[x][y]==WALL3)
        BlitImage(Vec2Si32(x*32, y*32),
                            panellSur, Vec4Si32(80, 120, 120, 160));
      else if(level[x][y]==WALL4)
        BlitImage(Vec2Si32(x*32, y*32),
                            panellSur, Vec4Si32(120, 120,
                                              160, 160));
      else if(level[x][y]==WALL5)
        BlitImage(Vec2Si32(x*32, y*32),
                            panellSur, Vec4Si32(240, 120, 280, 160));
      if((manx==x)&&(many==y))
        BlitImage(Vec2Si32(x*32, y*32),
                            panellSur, Vec4Si32(mantick*40, 160, 40+mantick*40, 200));
      if(level[x][y]==TVAR1)
        BlitImage(Vec2Si32(x*32, y*32),
                            panellSur, Vec4Si32(mantick*40, 200, 40+mantick*40, 240));
      else if(level[x][y]==TVAR2)
        BlitImage(Vec2Si32(x*32, y*32),
                            panellSur, Vec4Si32(mantick*40, 240, 40+mantick*40, 280));
      else if(level[x][y]==TVAR3)
        BlitImage(Vec2Si32(x*32, y*32),
                            panellSur, Vec4Si32(mantick*40, 360, 40+mantick*40, 400));
      else if(level[x][y]==TVAR4)
        BlitImage(Vec2Si32(x*32, y*32),
                            panellSur, Vec4Si32(120+wallredraw[x][y]*40, 120,
                                              160+wallredraw[x][y]*40, 160));
      else if(level[x][y]==TVAR5)
        BlitImage(Vec2Si32(x*32, y*32),
                            panellSur, Vec4Si32(mantick*40, 440, 40+mantick*40, 480));
      else if(level[x][y]==TVAR6)
        BlitImage(Vec2Si32(x*32, y*32),
                            panellSur, Vec4Si32(160+mantick*40, 160, 200+mantick*40, 200));
      if(prizes[x][y]==P1LIFE)
        BlitImage(Vec2Si32(x*32, y*32),
                            panellSur, Vec4Si32(mantick*40, 280, 40+mantick*40, 320));
      else if(prizes[x][y]==P1BOM2)
      {
        BlitImage(Vec2Si32(x*32, y*32),
                            panellSur, Vec4Si32(mantick*40, 320, 40+mantick*40, 360));
        BlitImage(Vec2Si32(x*32+25, y*32+5),
                            panellSur, Vec4Si32(631,355,640,368));
      }
      else if(prizes[x][y]==P5BOM2)
      {
        BlitImage(Vec2Si32(x*32, y*32),
                            panellSur, Vec4Si32(mantick*40, 320, 40+mantick*40, 360));
        BlitImage(Vec2Si32(x*32+25, y*32+5),
                            panellSur, Vec4Si32(631,411,640,424));
      }
      else if(prizes[x][y]==P1BOM3)
      {
        BlitImage(Vec2Si32(x*32, y*32),
                            panellSur, Vec4Si32(160+mantick*40, 280,
                                              200+mantick*40, 320));
        BlitImage(Vec2Si32(x*32+25, y*32+5),
                            panellSur, Vec4Si32(631,355,640,368));
      }
      else if(prizes[x][y]==P5BOM3)
      {
        BlitImage(Vec2Si32(x*32, y*32),
                            panellSur, Vec4Si32(160+mantick*40, 280,
                                              200+mantick*40, 320));
        BlitImage(Vec2Si32(x*32+25, y*32+5),
                            panellSur, Vec4Si32(631,411,640,424));
      }
    }
  
  EditorBlitPanell();
}


void EditorOnTimer() {
  mouseCader++;
  if(mouseCader>2)
    mouseCader=0;
  tick++;
  if(tick>2)
    tick=1;
  mantick++;
  if(mantick>3)
    mantick=0;
  t1SholdMove = false;
  if (tick==1)
    t1SholdMove = true;
  
  Clear();
  WallRedraw();
  
  EditorPaintScreen();
}

void EditorOnKeyDown(Ui32 wParam) {
  switch (wParam) {
    case kKeyEscape:
      doExit = true;
      break;
  }
  Clear();
  EditorPaintScreen();
  return;
}

void EditorAffectMouse() {
  exitPressed=false;
  newPressed=false;
  levelPressed=false;
  tvarusPressed=false;
  if((mousex>502)&&(mousex<628)&&
     (mousey>8)&&(mousey<42)&&(lButtonDown))
    tvarusPressed=true;
  if((mousex>500)&&(mousex<566)) {
    // mouse is in buttons zone
    if((mousey>405)&&(mousey<429)) {
      // mouse is on 'Level'
      if(lButtonDown)
        levelPressed=true;
    }
    if((mousey>432)&&(mousey<456)) {
      // mouse is on 'Exit'
      if(lButtonDown)
        exitPressed=true;
    }
    if((mousey>378)&&(mousey<402)) {
      // mouse is on 'New'
      if(lButtonDown)
        newPressed=true;
    }
  }
  else if((lButtonDown)&&(labx>0)&&(laby>0)&&(labx<14)&&(laby<13)&&!((many==laby)&&(manx==labx))) {
    // put obj
    if(obj==CLEAR) {
      level[labx][laby]=CLEAR;
      prizes[labx][laby]=CLEAR;
    } else if(obj==WALL) {
      level[labx][laby]=WALL;
    } else if(obj==WALL2) {
      level[labx][laby]=WALL2;
    } else if(obj==WALL3) {
      level[labx][laby]=WALL3;
    } else if(obj==WALL4) {
      level[labx][laby]=WALL4;
    } else if(obj==WALL5) {
      level[labx][laby]=WALL5;
    } else if(obj==TVAR1) {
      level[labx][laby]=TVAR1;
    } else if(obj==TVAR2) {
      level[labx][laby]=TVAR2;
    } else if(obj==TVAR3) {
      level[labx][laby]=TVAR3;
    } else if(obj==TVAR4) {
      level[labx][laby]=TVAR4;
    } else if(obj==TVAR5) {
      level[labx][laby]=TVAR5;
    } else if(obj==TVAR6) {
      level[labx][laby]=TVAR6;
    } else if (obj==P1LIFE) {
      prizes[labx][laby]=P1LIFE;
    } else if(obj==P1BOM2) {
      prizes[labx][laby]=P1BOM2;
    } else if(obj==P5BOM2) {
      prizes[labx][laby]=P5BOM2;
    } else if(obj==P1BOM3) {
      prizes[labx][laby]=P1BOM3;
    } else if(obj==P5BOM3) {
      prizes[labx][laby]=P5BOM3;
    } else if(obj==MAN) {
      manx=labx;
      many=laby;
      level[labx][laby]=CLEAR;
      prizes[labx][laby]=CLEAR;
    }
  } else if((lButtonDown)&& ((labx==0)||(laby==0)||(labx==14)||(laby==13))) {
    if(obj==WALL) {
      level[labx][laby]=WALL;
    } else if(obj==WALL3) {
      level[labx][laby]=WALL3;
    }
  } else if((rButtonDown)&&(labx<14)&&(laby<13)&&(laby>0)&&(labx>0)) {
    level[labx][laby]=CLEAR;
    prizes[labx][laby]=CLEAR;
  }
  Clear();
  EditorPaintScreen();
}

void EditorOnMouseMove(Si32 x, Si32 y) {
  oldMousex=mousex;
  oldMousey=mousey;
  mousex = x;
  mousey = y;
  labx=int(float(mousex-15)/float(30));
  laby=int(float(mousey-15)/float(30));
  EditorAffectMouse();
  return;
}

void EditorOnRButtonDown(Si32 x, Si32 y) {
  // sound: click !
  clickSnd.Play();
  
  rButtonDown=true;
  EditorAffectMouse();
  return;
}

void EditorOnRButtonUp(Si32 x, Si32 y) {
  // sound: unclick !
  unclickSnd.Play();
  
  rButtonDown=false;
}

void EditorOnLButtonDown(Si32 x, Si32 y) {
  // sound: click !
  clickSnd.Play();
  
  lButtonDown=true;
  EditorAffectMouse();
  return;
}

void EditorLoadLevel(int levelNum) {
  int x;
  int y;
  for (y=0; y < 14; y++)
    for (x=0; x < 15; x++)
    {
      level[x][y] = CLEAR;
      prizes[x][y]= CLEAR;
    }
  
  LoadLevelFile(levelNum);
  
  for (y=0; y < 14; y++)
    for (x=0; x < 15; x++)
    {
      if (level[x][y] == MAN)
      {
        level[x][y]=CLEAR;
        manx=x;
        many=y;
      }
    }
  
  return;
}

void EditorNewMap() {
  levelNum=1;
  EditorLoadLevel(levelNum);
}

void EditorNextMap() {
  levelNum++;
  if(levelNum>13)
    levelNum=1;
  EditorLoadLevel(levelNum);
  Clear();
  EditorPaintScreen();
  return;
}

void EditorSaveMap() {
  int x;
  int y;
  char ch;
  
  //char fileName[]="data\\level00.dat";
  char fileName[128];
  sprintf(fileName, "data/level%d.dat", levelNum);
  
  std::ofstream foff(fileName);
  level[manx][many]=MAN;
  for (y=0; y < 14; y++)
    for (x=0; x < 15; x++)
    {
      ch=level[x][y];
      foff << ch;
    }
  for (y=0; y < 14; y++)
    for (x=0; x < 15; x++)
    {
      ch=prizes[x][y];
      foff << ch;
    }
  foff.close();
  level[manx][many]=CLEAR;
}


void EditorOnLButtonUp(Si32 mmx, Si32 mmy) {
  // sound: unclick !
  unclickSnd.Play();
  
  lButtonDown=false;
  if(exitPressed)
    doExit = true;
  if(newPressed)
    EditorNewMap();
  if(levelPressed)
    EditorNextMap();
  if(tvarusPressed)
    EditorSaveMap();
  EditorAffectMouse();
  int y=0;
  int x=0;
  if(mousey>150)
  {
    if(mousey<190)
      y=1;
    else if(mousey<230)
      y=2;
    else if(mousey<270)
      y=3;
    else if(mousey<310)
      y=4;
    else if(mousey<350)
      y=5;
    else if(mousey<390)
      y=6;
    else if(mousey<430)
      y=7;
    if(mousex>500)
    {
      if(mousex<540)
        x=1;
      else if(mousex<580)
        x=2;
      else if(mousex<620)
        x=3;
    }
  }
  if(y==1)
  {
    if(x==1)
      obj=WALL;
    else if(x==2)
      obj=WALL2;
    else if(x==3)
      obj=WALL3;
  }
  else if(y==2)
  {
    if(x==1)
      obj=WALL4;
    else if(x==2)
      obj=WALL5;
    else if(x==3)
      obj=MAN;
  }
  else if(y==3)
  {
    if(x==1)
      obj=TVAR1;
    else if(x==2)
      obj=TVAR2;
    else if(x==3)
      obj=TVAR3;
  }
  else if(y==4)
  {
    if(x==1)
      obj=TVAR4;
    else if(x==2)
      obj=TVAR5;
    else if(x==3)
      obj=TVAR6;
  }
  else if(y==5)
  {
    if(x==1)
      obj=P1LIFE;
    else if(x==2)
      obj=P1BOM2;
    else if(x==3)
      obj=P5BOM2;
  }
  else if((y==6)&&(x==3))
    obj=P1BOM3;
  else if((y==7)&&(x==3))
    obj=P5BOM3;
  return;
}

void LevelEditor() {
  EditorNewMap();
  
  double t1 = Time();
  double t2 = Time();
  double timerAcc = 0.0;
  while (!doExit) {
    t1 = t2;
    t2 = Time();
    double dt = t2 - t1;
    
    timerAcc += dt;
    if (timerAcc > kUpdatePeriod) {
      timerAcc = 0.0;
      EditorOnTimer();
    }
  
    for (Si32 i = 0; i < InputMessageCount(); ++i) {
      const InputMessage& msg = GetInputMessage(i);
      if (msg.kind == InputMessage::kKeyboard) {
        if (msg.keyboard.key_state == 1) {
          EditorOnKeyDown(msg.keyboard.key);
        }
      } else {
        EditorOnMouseMove(MousePos().x, ScreenSize().y - 1 -MousePos().y);
        if (msg.keyboard.key == kKeyMouseLeft) {
          if (msg.keyboard.key_state == 1) {
            EditorOnLButtonDown(MousePos().x, ScreenSize().y - 1 - MousePos().y);
          } else {
            EditorOnLButtonUp(MousePos().x, ScreenSize().y - 1 - MousePos().y);
          }
        } else if (msg.keyboard.key == kKeyMouseRight) {
          if (msg.keyboard.key_state == 1) {
            EditorOnRButtonDown(MousePos().x, ScreenSize().y - 1 - MousePos().y);
          } else {
            EditorOnRButtonUp(MousePos().x, ScreenSize().y - 1 - MousePos().y);
          }
        }
      }
    }
    
    LimitRate(t1);
    ShowFrame();
  }
  LoadLevel(levelNum);
  doExit = false;
}


void EasyMain() {
  ResizeScreen(640, 480);
  SetCursorVisible(false);
  
  std::vector<Ui8> data = ReadFile("data/leveleditor.dat", true);
  if (data.size() == 1 && data[0] == 1) {
    isLevelEditorUnlocked = true;
  }

  InitGame();
  InitSound();
  InitGraphics();
  
  double t1 = Time();
  double t2 = Time();
  double timerAcc = 0.0;
  while (!doExit) {
    t1 = t2;
    t2 = Time();
    double dt = t2 - t1;
    
    if (!pauseMode) {
      timerAcc += dt;
      if (timerAcc > kUpdatePeriod) {
        timerAcc = 0.0;
        if (!exitAfterKey) {
          OnTimer();
        } else {
          Clear();
          SuperTimer();
        }
      }
    }
  
    for (Si32 i = 0; i < InputMessageCount(); ++i) {
      const InputMessage& msg = GetInputMessage(i);
      if (msg.kind == InputMessage::kKeyboard) {
        if (msg.keyboard.key_state == 1) {
          OnKeyDown(msg.keyboard.key);
        }
      } else {
        OnMouseMove(MousePos().x, ScreenSize().y - 1 -MousePos().y);
        if (msg.keyboard.key == kKeyMouseLeft) {
          if (msg.keyboard.key_state == 1) {
            OnLButtonDown(MousePos().x, ScreenSize().y - 1 - MousePos().y);
          } else {
            OnLButtonUp(MousePos().x, ScreenSize().y - 1 - MousePos().y);
          }
        }
      }
    }
    
    LimitRate(t1);
    ShowFrame();
  }
}

bool InitGraphics() {
	// Load panell Dibs
	panellSur.Load("data/panel.tga");
	// Load cool Dibs
	coolSur.Load("data/cool.tga");
  
  Clear();
	PaintScreen();
  return true;
}

void OnKeyDown(Ui32 wParam) {
	if (exitAfterKey) {
    doExit = true;
		return;
	}
	if (coolOnline)
		return;
	if (pauseMode) {
		if (wParam == kKeyEnter) {
			UnPause();
			return;
		}
		return;
	}
	int XA=0;
	int YA=0;
	switch (wParam)
	{

	case kKeyEscape:
      doExit = true;
		break;

	case kKeyDown:
		YA=1;
		break;
		
	case kKeyUp:
		YA=-1;
		break;
		
	case kKeyLeft:
		XA=-1;
		break;
		
	case kKeyRight:
		XA=1;
		break;
		
	case kKeySpace:
		if (bombs[manx][many]==CLEAR) {
			if (score<=0)
				break;
			bombs[manx][many]=BOMB;
			bomtim[manx][many]=20;
			score--;
		}
		break;

	case 'Q':
		if (bombs[manx][many]==CLEAR) {
			if (bomb2num<=0)
				break;
			bombs[manx][many]=BOMB2;
			bomtim[manx][many]=20;
			bomb2num--;
		}
		break;

	case 'A':
		if (bombs[manx][many]==CLEAR) {
			if ((curputbomb3>=5)||(bomb3num<=0)) {
				curputbomb3=0;
				expbomb3 = true;
				break;
			}
			bombs[manx][many]=BOMB3;
			bomtim[manx][many]=50;
			bomb3num--;
			curputbomb3++;
		}
		break;
	}
		
	if ((level[manx+XA][many+YA]==CLEAR)&&
		(creatures[manx+XA][many+YA]==CLEAR)&&
		(bombs[manx+XA][many+YA]==CLEAR))
	{
		manx+=XA;
		many+=YA;
		if (prizes[manx][many]==P1LIFE)
			lifes++;
		else if (prizes[manx][many]==P5BOM2)
			bomb2num+=5;
		else if (prizes[manx][many]==P1BOM2)
			bomb2num++;
		else if (prizes[manx][many]==P5BOM3)
			bomb3num+=5;
		else if (prizes[manx][many]==P1BOM3)
			bomb3num++;
		prizes[manx][many]=CLEAR;
	}
  Clear();
	PaintScreen();
	return;
}

void PaintScreen() {
	for (int y=0; y < 14; y++)
		for (int x=0; x < 15; x++) {
		if (prizes[x][y]==P1LIFE)
			BlitImage(Vec2Si32(x*32, y*32), panellSur, Vec4Si32(mantick*40, 280, 40+mantick*40, 320));
		else if ((prizes[x][y]==P5BOM2)|| (prizes[x][y]==P1BOM2))
			BlitImage(Vec2Si32(x*32, y*32),
			panellSur, Vec4Si32(mantick*40, 320, 40+mantick*40, 360));
		else if ((prizes[x][y]==P5BOM3)|| (prizes[x][y]==P1BOM3))
			BlitImage(Vec2Si32(x*32, y*32), panellSur, Vec4Si32(160+mantick*40, 280, 200+mantick*40, 320));
		if (level[x][y]==WALL)
			BlitImage(Vec2Si32(x*32, y*32), panellSur, Vec4Si32(0, 120, 40, 160));
		else if (level[x][y]==WALL2)
			BlitImage(Vec2Si32(x*32, y*32), panellSur, Vec4Si32(40, 120, 80, 160));
		else if (level[x][y]==WALL3)
			BlitImage(Vec2Si32(x*32, y*32), panellSur, Vec4Si32(80, 120, 120, 160));
		else if (level[x][y]==WALL4)
			BlitImage(Vec2Si32(x*32, y*32), panellSur, Vec4Si32(120+wallredraw[x][y]*40, 120, 160+wallredraw[x][y]*40, 160));
		else if (level[x][y]==WALL5)
			BlitImage(Vec2Si32(x*32, y*32), panellSur, Vec4Si32(240, 120, 280, 160));
		if (bombs[x][y]==BOMB)
			BlitImage(Vec2Si32(x*32, y*32), panellSur, Vec4Si32(
			400-int(bomtim[x][y]/2)*40, 0, 400-int(bomtim[x][y]/2)*40+40, 40));
		else if (bombs[x][y]==BOMB2)
			BlitImage(Vec2Si32(x*32, y*32), panellSur, Vec4Si32(
			400-int(bomtim[x][y]/2)*40, 40, 400-int(bomtim[x][y]/2)*40+40, 80));
		else if (bombs[x][y]==BOMB3) {
			if (bomtim[x][y]==50)
				BlitImage(Vec2Si32(x*32, y*32), panellSur, Vec4Si32(400, 240,440, 280));
			else
        BlitImage(Vec2Si32(x*32, y*32), panellSur, Vec4Si32(
          400-int(bomtim[x][y]/2)*40, 240, 400-int(bomtim[x][y]/2)*40+40, 280));
		}
		if ((manx==x)&&(many==y))
			BlitImage(Vec2Si32(x*32, y*32), panellSur, Vec4Si32(mantick*40, 160, 40+mantick*40, 200));
		if (creatures[x][y]==TVAR1)
			BlitImage(Vec2Si32(x*32, y*32), panellSur, Vec4Si32(mantick*40, 200, 40+mantick*40, 240));
		else if (creatures[x][y]==TVAR2)
			BlitImage(Vec2Si32(x*32, y*32), panellSur, Vec4Si32(mantick*40, 240, 40+mantick*40, 280));
		else if (creatures[x][y]==TVAR3)
			BlitImage(Vec2Si32(x*32, y*32), panellSur, Vec4Si32(mantick*40, 360, 40+mantick*40, 400));
		else if (creatures[x][y]==TVAR4)
			BlitImage(Vec2Si32(x*32, y*32), panellSur, Vec4Si32(120+wallredraw[x][y]*40, 120, 160+wallredraw[x][y]*40, 160));
		else if (creatures[x][y]==TVAR5)
			BlitImage(Vec2Si32(x*32, y*32), panellSur, Vec4Si32(mantick*40, 440, 40+mantick*40, 480));
		else if (creatures[x][y]==TVAR6)
			BlitImage(Vec2Si32(x*32, y*32), panellSur, Vec4Si32(160+mantick*40, 160, 200+mantick*40, 200));
		else if (creatures[x][y]==TRANS2)
			BlitImage(Vec2Si32(x*32, y*32), panellSur, Vec4Si32(160-transtim[x][y]*40, 400, 200-transtim[x][y]*40, 440));
		else if (creatures[x][y]==TRANS5)
			BlitImage(Vec2Si32(x*32, y*32), panellSur, Vec4Si32(320-transtim[x][y]*40, 200, 360-transtim[x][y]*40, 240));
		if (explode[x][y]==BOOM)
			BlitImage(Vec2Si32(x*32, y*32), panellSur, Vec4Si32(exptim[x][y]*40-40, 80, exptim[x][y]*40, 120));
	}

	BlitPanell();
}

void InitGame() {
	LoadLevel(1);
	return;
}

void LoadLevel(int levelNum) {
	if (levelNum==14) {
		GameComplete();
		return;
	}
	int x;
	int y;
	for (y=0; y < 14; y++)
		for (x=0; x < 15; x++) {
		level[x][y] = CLEAR;
		bombs[x][y] = CLEAR;
		explode[x][y] = CLEAR;
		creatures[x][y] = CLEAR;
		prizes[x][y]= CLEAR;
	}

	LoadLevelFile(levelNum);

	for (y=0; y < 14; y++)
		for (x=0; x < 15; x++) {
		if (level[x][y] == TVAR1) {
			level[x][y]=CLEAR;
			creatures[x][y]=TVAR1;
		} else if (level[x][y] == TVAR2) {
			level[x][y]=CLEAR;
			creatures[x][y]=TVAR2;
		} else if (level[x][y] == TVAR3) {
			level[x][y]=CLEAR;
			creatures[x][y]=TVAR3;
		} else if (level[x][y] == TVAR4) {
			level[x][y]=CLEAR;
			creatures[x][y]=TVAR4;
		} else if (level[x][y] == TVAR5) {
			level[x][y]=CLEAR;
			creatures[x][y]=TVAR5;
		} else if (level[x][y] == TVAR6) {
			level[x][y]=CLEAR;
			creatures[x][y]=TVAR6;
		} else if (level[x][y] == MAN) {
			level[x][y]=CLEAR;
			manx=x;
			many=y;
		}
	}
	return;
}

void LoadLevelFile(int levelNum) {
	int x;
	int y;
	char ch;

  char fileName[128];
  sprintf(fileName, "data/level%d.dat", levelNum); //new

  std::ifstream fin(fileName);
	for (y=0; y < 14; y++)
		for (x=0; x < 15; x++) {
		fin >> ch;
		level[x][y] = ch;
	}
	for (y=0; y < 14; y++)
		for (x=0; x < 15; x++) {
		fin >> ch;
		prizes[x][y] = ch;
	}
	fin.close();
}

void CBomTim() {
	int x;
	int y;
	for (y=0; y < 14; y++)
		for (x=0; x < 15; x++) {
		if (bombs[x][y]==BOMB) {
			bomtim[x][y]--;
			if (bomtim[x][y]==0){
				bombs[x][y]=CLEAR;
				BombExplode(x,y);
			}
		} else if (bombs[x][y]==BOMB2) {
			bomtim[x][y]--;
			if (bomtim[x][y]==0) {
				bombs[x][y]=CLEAR;
				Bomb2Explode(x,y);
			}
		} else if (bombs[x][y]==BOMB3) {
			if (bomtim[x][y]<50) {
				bomtim[x][y]--;
				if (bomtim[x][y]==0) {
					bombs[x][y]=CLEAR;
					Bomb3Explode(x,y);
				}
			}
			else if ((bomtim[x][y]==50)&&
				(expbomb3))
				bomtim[x][y]=12;
		}
	}

	for (x=0; x<15; x++)
		for (y=0; y<14; y++)
		if (explode[x][y]==BOOM) {
			exptim[x][y]--;
			if (exptim[x][y]==0) {
				explode[x][y]=CLEAR;
			}
		}

	if (t1SholdMove == true)
		TvarusesMove();
  Clear();
	PaintScreen();
	expbomb3 = false;
	return;
}

void TvarusesMove() {
	int x;
	int y;
	int xm;
	int ym;
	int temp;
	bool cont;
	bool bombed;
	bool rfbomb;
	int run = 0;

	for (x=0; x<15; x++)
		for (y=0; y<14; y++)
		creatures2[x][y]=CLEAR;

	for (x=0; x<15; x++)
		for (y=0; y<14; y++) {
		if (creatures[x][y]==TVAR1) {
			xm=(rand() % 5)-1;
			ym=0;
			if (xm>1) {
				xm=0;
				ym=(rand() % 3)-1;
			}

			if (manx==x) {
				cont = true;
				if (many<y) {
					for (temp=y;temp>many;temp--) {
						if (level[x][temp]!=CLEAR)
							cont = false;
					}
					if (cont == true) {
						ym=-1;
						xm=0;
					}
				}
				if (many>y) {
					for (temp=y;temp<many;temp++) {
						if (level[x][temp]!=CLEAR)
							cont = false;
					}
					if (cont == true) {
						ym=1;
						xm=0;
					}
				}
			}

			if (many==y) {
				cont = true;
				if (manx<x) {
					for (temp=x;temp>manx;temp--) {
						if (level[temp][y]!=CLEAR)
							cont = false;
					}
					if (cont == true) {
						xm=-1;
						ym=0;
					}
				}
				if (manx>x) {
					for (temp=x;temp<manx;temp++) {
						if (level[temp][y]!=CLEAR)
							cont = false;
					}
					if (cont == true) {
						xm=1;
						ym=0;
					}
				}
			}
			if ((level[x+xm][y+ym]==CLEAR)&&
				(creatures[x+xm][y+ym]==CLEAR)&&
				(creatures2[x+xm][y+ym]==CLEAR))
				creatures2[x+xm][y+ym]=TVAR1;
			else if (creatures[x+xm][y+ym]==TVAR4) {
				creatures[x+xm][y+ym]=TRANS2;
				creatures2[x+xm][y+ym]=TRANS2;
				transtim[x+xm][y+ym]=5;
			}
			else
        creatures2[x][y]=TVAR1;
		} else if (creatures[x][y]==TVAR2) {
			////////////////////////////////////////
			//if it doesnt see anything it randoms//
			////////////////////////////////////////
			xm=(rand() % 5)-1;
			ym=0;
			if (xm>1) {
				xm=0;
				ym=(rand() % 3)-1;
			}
			///////////////////////////////////
			//if it sees bomb it runs from it//
			///////////////////////////////////
			rfbomb = false;

			bombed = false;
			cont = true;
			for (temp=y;temp>0;temp--) {
				if (level[x][temp]!=CLEAR)
					cont = false;
				if ((bombs[x][temp]!=CLEAR)&&
					(bombs[x][temp]!=BOMB3)&&(cont))
					bombed = true;
			}
			if (bombed) {
				rfbomb = true;
				run=6;
			}

			bombed = false;
			cont = true;
			for (temp=y;temp<14;temp++) {
				if (level[x][temp]!=CLEAR)
					cont = false;
				if ((bombs[x][temp]!=CLEAR)&&
					(bombs[x][temp]!=BOMB3)&&(cont))
					bombed = true;
			}
			if (bombed) {
				rfbomb = true;
				run=4;
			}

			bombed = false;
			cont = true;
			for (temp=x;temp>0;temp--) {
				if (level[temp][y]!=CLEAR)
					cont = false;
				if ((bombs[temp][y]!=CLEAR)&&
					(bombs[temp][y]!=BOMB3)&&(cont))
					bombed = true;
			} if (bombed) {
				rfbomb = true;
				run=8;
			}

			bombed = false;
			cont = true;
			for (temp=x;temp<15;temp++) {
				if (level[temp][y]!=CLEAR)
					cont = false;
				if ((bombs[temp][y]!=CLEAR)&&
					(bombs[temp][y]!=BOMB3)&&(cont))
					bombed = true;
			} if (bombed) {
				rfbomb = true;
				run=2;
			}

			if (rfbomb) {
				if (run==8) {
					ym=0;
					xm=1;
				} else if (run==2) {
					ym=0;
					xm=-1;
				} else if (run==4) {
					xm=0;
					ym=-1;
				} else {
					xm=0;
					ym=1;
				}
			}

			/////////////////////////////////
			//if it sees man it runs to him//
			/////////////////////////////////
			if (manx==x) {
				cont = true;
				if (many<y) {
					for (temp=y;temp>many;temp--) {
						if (level[x][temp]!=CLEAR)
							cont = false;
					}
					if (cont == true) {
						ym=-1;
						xm=0;
					}
				}
				if (many>y) {
					for (temp=y;temp<many;temp++) {
						if (level[x][temp]!=CLEAR)
							cont = false;
					}
					if (cont == true) {
						ym=1;
						xm=0;
					}
				}
			}

			if (many==y) {
				cont = true;
				if (manx<x) {
					for (temp=x;temp>manx;temp--) {
						if (level[temp][y]!=CLEAR)
							cont = false;
					}
					if (cont == true) {
						xm=-1;
						ym=0;
					}
				}
				if (manx>x) {
					for (temp=x;temp<manx;temp++) {
						if (level[temp][y]!=CLEAR)
							cont = false;
					}
					if (cont == true) {
						xm=1;
						ym=0;
					}
				}
			}
			if ((level[x+xm][y+ym]==CLEAR)&&
				(creatures[x+xm][y+ym]==CLEAR)&&
				(creatures2[x+xm][y+ym]==CLEAR))
				creatures2[x+xm][y+ym]=TVAR2;
			else
        creatures2[x][y]=TVAR2;
		} else if (creatures[x][y]==TVAR3) {
			////////////////////////////////////////
			//if it doesnt see anything it randoms//
			////////////////////////////////////////
			xm=(rand() % 5)-1;
			ym=0;
			if (xm>1) {
				xm=0;
				ym=(rand() % 3)-1;
			}
			///////////////////////////////////
			//if it sees bomb it runs from it//
			///////////////////////////////////
			rfbomb = false;

			bombed = false;
			cont = true;
			for (temp=y;temp>0;temp--) {
				if (level[x][temp]!=CLEAR)
					cont = false;
				if ((bombs[x][temp]!=CLEAR)&&(cont))
					bombed = true;
			}
			if (bombed) {
				rfbomb = true;
				run=6;
			}

			bombed = false;
			cont = true;
			for (temp=y;temp<14;temp++) {
				if (level[x][temp]!=CLEAR)
					cont = false;
				if ((bombs[x][temp]!=CLEAR)&&(cont))
					bombed = true;
			}
			if (bombed) {
				rfbomb = true;
				run=4;
			}

			bombed = false;
			cont = true;
			for (temp=x;temp>0;temp--) {
				if (level[temp][y]!=CLEAR)
					cont = false;
				if ((bombs[temp][y]!=CLEAR)&&(cont))
					bombed = true;
			}
			if (bombed) {
				rfbomb = true;
				run=8;
			}

			bombed = false;
			cont = true;
			for (temp=x;temp<15;temp++) {
				if (level[temp][y]!=CLEAR)
					cont = false;
				if ((bombs[temp][y]!=CLEAR)&&(cont))
					bombed = true;
			}
			if (bombed) {
				rfbomb = true;
				run=2;
			}

			if (rfbomb) {
				if (run==8) {
					ym=0;
					xm=1;
				} else if (run==2) {
					ym=0;
					xm=-1;
				} else if (run==4) {
					xm=0;
					ym=-1;
				} else {
					xm=0;
					ym=1;
				}

				if ((level[x+xm][y+ym]!=CLEAR)||
            (creatures[x+xm][y+ym]!=CLEAR)||
            (creatures2[x+xm][y+ym]!=CLEAR)) {
					//there is something !!!
					if (xm==0) {
						xm=(rand() % 3)-1;
						ym=0;
					} else {
						xm=(rand() % 3)-1;
						ym=0;
					}
				}
			}

			/////////////////////////////////
			//if it sees man it runs to him//
			/////////////////////////////////
			if (manx==x) {
				cont = true;
				if (many<y) {
					for (temp=y;temp>many;temp--) {
						if (level[x][temp]!=CLEAR)
							cont = false;
					}
					if (cont == true) {
						ym=-1;
						xm=0;
					}
				}
				if (many>y) {
					for (temp=y;temp<many;temp++) {
						if (level[x][temp]!=CLEAR)
							cont = false;
					}
					if (cont == true) {
						ym=1;
						xm=0;
					}
				}
			}

			if (many==y) {
				cont = true;
				if (manx<x) {
					for (temp=x;temp>manx;temp--) {
						if (level[temp][y]!=CLEAR)
							cont = false;
					}
					if (cont == true) {
						xm=-1;
						ym=0;
					}
				}
				if (manx>x) {
					for (temp=x;temp<manx;temp++) {
						if (level[temp][y]!=CLEAR)
							cont = false;
					}
					if (cont == true) {
						xm=1;
						ym=0;
					}
				}
			}
			if ((level[x+xm][y+ym]==CLEAR)&&
				(creatures[x+xm][y+ym]==CLEAR)&&
				(creatures2[x+xm][y+ym]==CLEAR))
				creatures2[x+xm][y+ym]=TVAR3;
			else if (creatures[x+xm][y+ym]==TVAR4) {
				creatures[x+xm][y+ym]=TRANS5;
				creatures2[x+xm][y+ym]=TRANS5;
				transtim[x+xm][y+ym]=4;
			}
			else
        creatures2[x][y]=TVAR3;
		} else if (creatures[x][y]==TVAR4) {
			////////////////////////////////////////
			//if it doesnt see anything it randoms//
			////////////////////////////////////////
			xm=(rand() % 5)-1;
			ym=0;
			if (xm>1) {
				xm=0;
				ym=(rand() % 3)-1;
			}
			/////////////////////////////////
			//if it sees man it runs to him//
			/////////////////////////////////
			if (manx==x) {
				cont = true;
				if (many<y) {
					for (temp=y;temp>many;temp--) {
						if (level[x][temp]!=CLEAR)
							cont = false;
					}
					if (cont == true) {
						ym=-1;
						xm=0;
					}
				}
				if (many>y) {
					for (temp=y;temp<many;temp++) {
						if (level[x][temp]!=CLEAR)
							cont = false;
					}
					if (cont == true) {
						ym=1;
						xm=0;
					}
				}
			}

			if (many==y) {
				cont = true;
				if (manx<x) {
					for (temp=x;temp>manx;temp--) {
						if (level[temp][y]!=CLEAR)
							cont = false;
					}
					if (cont == true) {
						xm=-1;
						ym=0;
					}
				}
				if (manx>x) {
					for (temp=x;temp<manx;temp++) {
						if (level[temp][y]!=CLEAR)
							cont = false;
					}
					if (cont == true) {
						xm=1;
						ym=0;
					}
				}
			}

			///////////////////////////////////
			//if it sees bomb it runs from it//
			///////////////////////////////////
			rfbomb = false;

			bombed = false;
			cont = true;
			for (temp=y;temp>0;temp--) {
				if (level[x][temp]!=CLEAR)
					cont = false;
				if ((bombs[x][temp]!=CLEAR)&&(cont))
					bombed = true;
			}
			if (bombed) {
				rfbomb = true;
				run=6;
			}

			bombed = false;
			cont = true;
			for (temp = y; temp < 14; temp++) {
				if (level[x][temp]!=CLEAR)
					cont = false;
				if ((bombs[x][temp]!=CLEAR)&&(cont))
					bombed = true;
			}
			if (bombed) {
				rfbomb = true;
				run=4;
			}

			bombed = false;
			cont = true;
			for (temp = x; temp > 0; temp--) {
				if (level[temp][y]!=CLEAR)
					cont = false;
				if ((bombs[temp][y]!=CLEAR)&&(cont))
					bombed = true;
			}
			if (bombed) {
				rfbomb = true;
				run=8;
			}

			bombed = false;
			cont = true;
			for (temp=x;temp<15;temp++) {
				if (level[temp][y]!=CLEAR)
					cont = false;
				if ((bombs[temp][y]!=CLEAR)&&(cont))
					bombed = true;
			}
			if (bombed) {
				rfbomb = true;
				run=2;
			}

			if (rfbomb) {
				if (run==8) {
					ym=0;
					xm=1;
				} else if (run==2) {
					ym=0;
					xm=-1;
				} else if (run==4) {
					xm=0;
					ym=-1;
				} else if (run==6) {
					xm=0;
					ym=1;
				}

				if ((level[x+xm][y+ym]!=CLEAR)||
            (creatures[x+xm][y+ym]!=CLEAR)||
            (creatures2[x+xm][y+ym]!=CLEAR)) {
					//there is something !!!
					if (xm==0) {
						xm=(rand() % 3)-1;
						ym=0;
					} else {
						xm=(rand() % 3)-1;
						ym=0;
					}
				}
			}

			if ((level[x+xm][y+ym]==CLEAR)&&
          (creatures[x+xm][y+ym]==CLEAR)&&
          (creatures2[x+xm][y+ym]==CLEAR)&&
          ((manx!=x+xm)||(many!=y+ym)))
				creatures2[x+xm][y+ym]=TVAR4;
			else
        creatures2[x][y]=TVAR4;
		} else if (creatures[x][y]==TVAR5) {
			////////////////////////////////////////
			//if it doesnt see anything it randoms//
			////////////////////////////////////////
			xm = (rand() % 5)-1;
			ym = 0;
			if (xm > 1) {
				xm=0;
				ym=(rand() % 3)-1;
			}
			///////////////////////////////////
			//if it sees bomb it runs from it//
			///////////////////////////////////
			rfbomb = false;

			bombed = false;
			cont = true;
			for (temp = y; temp > 0; temp--) {
				if (level[x][temp]!=CLEAR)
					cont = false;
				if ((bombs[x][temp]!=CLEAR)&&(cont))
					bombed = true;
			}
			if (bombed) {
				rfbomb = true;
				run=6;
			}

			bombed = false;
			cont = true;
			for (temp = y; temp < 14; temp++) {
				if (level[x][temp]!=CLEAR)
					cont = false;
				if ((bombs[x][temp]!=CLEAR)&&(cont))
					bombed = true;
			}
			if (bombed) {
				rfbomb = true;
				run=4;
			}

			bombed = false;
			cont = true;
			for (temp = x; temp > 0; temp--) {
				if (level[temp][y]!=CLEAR)
					cont = false;
				if ((bombs[temp][y]!=CLEAR)&&(cont))
					bombed = true;
			}
			if (bombed) {
				rfbomb = true;
				run=8;
			}

			bombed = false;
			cont = true;
			for (temp = x; temp < 15; temp++) {
				if (level[temp][y]!=CLEAR)
					cont = false;
				if ((bombs[temp][y]!=CLEAR)&&(cont))
					bombed = true;
			}
			if (bombed) {
				rfbomb = true;
				run=2;
			}

			if (rfbomb) {
				if ((run==8)||(run==2)) {
					xm=0;
					temp=(rand() % 2);
					if (temp==1)
						ym=-1;
					else
            ym=1;
				} else if ((run==4)||(run==6)) {
					ym=0;
					temp=(rand() % 2);
					if (temp==1)
						xm=-1;
					else xm=1;
				}

				if ((level[x+xm][y+ym]!=CLEAR)||
            (creatures[x+xm][y+ym]!=CLEAR)||
            (creatures2[x+xm][y+ym]!=CLEAR)) {
					//there is something !!!
					if (run==8) {
						ym=0;
						xm=1;
					} else if (run==2) {
						ym=0;
						xm=-1;
					} else if (run==4) {
						xm=0;
						ym=-1;
					} else {
						xm=0;
						ym=1;
					}
				}
			}

			/////////////////////////////////
			//if it sees man it runs to him//
			/////////////////////////////////
			if (manx==x) {
				cont = true;
				if (many<y) {
					for (temp=y;temp>many;temp--) {
						if (level[x][temp]!=CLEAR)
							cont = false;
					}
					if (cont == true) {
						ym=-1;
						xm=0;
					}
				}
				if (many>y) {
					for (temp=y;temp<many;temp++) {
						if (level[x][temp]!=CLEAR)
							cont = false;
					}
					if (cont == true) {
						ym=1;
						xm=0;
					}
				}
			}

			if (many==y) {
				cont = true;
				if (manx<x) {
					for (temp=x;temp>manx;temp--) {
						if (level[temp][y]!=CLEAR)
							cont = false;
					}
					if (cont == true) {
						xm=-1;
						ym=0;
					}
				}
				if (manx > x) {
					for (temp = x; temp < manx; temp++) {
						if (level[temp][y]!=CLEAR)
							cont = false;
					}
					if (cont == true) {
						xm=1;
						ym=0;
					}
				}
			}
			if ((level[x+xm][y+ym]==CLEAR)&&
          (creatures[x+xm][y+ym]==CLEAR)&&
          (creatures2[x+xm][y+ym]==CLEAR))
				creatures2[x+xm][y+ym]=TVAR5;
			else creatures2[x][y]=TVAR5;
		}

		/////////////////////////////////////////////////////////
		//tvarus6
		/////////
		else if (creatures[x][y]==TVAR6) {
			////////////////////////////////////////
			//if it doesnt see anything it randoms//
			////////////////////////////////////////
			xm=(rand() % 5)-1;
			ym=0;
			if (xm>1) {
				xm=0;
				ym=(rand() % 3)-1;
			}
			/////////////////////////////////
			//if it sees man it runs to him//
			/////////////////////////////////
			if (manx==x) {
				cont = true;
				if (many<y) {
					for (temp=y;temp>many;temp--) {
						if (level[x][temp]!=CLEAR)
							cont = false;
					}
					if (cont == true) {
						ym=-1;
						xm=0;
					}
				}
				if (many>y) {
					for (temp=y;temp<many;temp++) {
						if (level[x][temp]!=CLEAR)
							cont = false;
					}
					if (cont == true) {
						ym=1;
						xm=0;
					}
				}
			}

			if (many==y) {
				cont = true;
				if (manx < x) {
					for (temp=x;temp>manx;temp--) {
						if (level[temp][y]!=CLEAR)
							cont = false;
					}
					if (cont == true) {
						xm=-1;
						ym=0;
					}
				}
				if (manx>x) {
					for (temp=x;temp<manx;temp++) {
						if (level[temp][y]!=CLEAR)
							cont = false;
					}
					if (cont == true) {
						xm=1;
						ym=0;
					}
				}
			}

			///////////////////////////////////
			//if it sees bomb it runs from it//
			///////////////////////////////////
			rfbomb = false;

			bombed = false;
			cont = true;
			for (temp = y; temp > 0; temp--) {
				if (level[x][temp]!=CLEAR)
					cont = false;
				if ((bombs[x][temp]!=CLEAR)&&(cont))
					bombed = true;
			}
			if (bombed) {
				rfbomb = true;
				run=6;
			}

			bombed = false;
			cont = true;
			for (temp = y; temp < 14; temp++) {
				if (level[x][temp]!=CLEAR)
					cont = false;
				if ((bombs[x][temp]!=CLEAR)&&(cont))
					bombed = true;
			}
			if (bombed) {
				rfbomb = true;
				run=4;
			}

			bombed = false;
			cont = true;
			for (temp = x; temp > 0; temp--) {
				if (level[temp][y]!=CLEAR)
					cont = false;
				if ((bombs[temp][y]!=CLEAR)&&(cont))
					bombed = true;
			}
			if (bombed) {
				rfbomb = true;
				run=8;
			}

			bombed = false;
			cont = true;
			for (temp = x; temp < 15; temp++) {
				if (level[temp][y]!=CLEAR)
					cont = false;
				if ((bombs[temp][y]!=CLEAR)&&(cont))
					bombed = true;
			}
			if (bombed) {
				rfbomb = true;
				run=2;
			}

			if (rfbomb) {
				if ((run==8)||(run==2)) {
					xm=0;
					temp=(rand() % 2);
					if (temp==2)
						ym=-1;
					else ym=1;
				} else if ((run==4)||(run==6)) {
					ym=0;
					temp=(rand() % 2);
					if (temp==2)
						xm=-1;
					else xm=1;
				}

				if ((level[x+xm][y+ym]!=CLEAR)||
            (creatures[x+xm][y+ym]!=CLEAR)||
            (creatures2[x+xm][y+ym]!=CLEAR)) {
					//there is something !!!
					if (run==8) {
						ym=0;
						xm=1;
					} else if (run==2) {
						ym=0;
						xm=-1;
					} else if (run==4) {
						xm=0;
						ym=-1;
					} else {
						xm=0;
						ym=1;
					}
				}
			}

			if ((level[x+xm][y+ym]==CLEAR)&&
          (creatures[x+xm][y+ym]==CLEAR)&&
          (creatures2[x+xm][y+ym]==CLEAR))
				creatures2[x+xm][y+ym]=TVAR6;
			else
        creatures2[x][y]=TVAR6;
		} else if (creatures[x][y]==TRANS2) {
			transtim[x][y]--;
			creatures2[x][y]=TRANS2;
			if (transtim[x][y]==0) {
				creatures2[x][y]=TVAR2;
			}
		} else if (creatures[x][y]==TRANS5) {
			transtim[x][y]--;
			creatures2[x][y]=TRANS5;
			if (transtim[x][y]==0) {
				creatures2[x][y]=TVAR5;
			}
		}
	}
	for (x=0; x<15; x++)
		for (y=0; y<14; y++)
		if ((creatures[x][y]==CLEAR)||
      (creatures[x][y]==TVAR1)||
      (creatures[x][y]==TVAR2)||
      (creatures[x][y]==TVAR3)||
      (creatures[x][y]==TVAR4)||
      (creatures[x][y]==TVAR5)||
      (creatures[x][y]==TVAR6)||
      (creatures[x][y]==TRANS2)||
      (creatures[x][y]==TRANS5))
		creatures[x][y]=creatures2[x][y];
	return;
}

void BombExplode(int x,int y) {
	PlayBoomSnd();
	int XA;
	int YA;
	for (XA=-1; XA<2; XA++) {
		if ((level[x+XA][y]==CLEAR)||
			  (level[x+XA][y]==WALL2)||
			  (level[x+XA][y]==WALL4)) {
			if (bomtim[x+XA][y]==50)
				bomtim[x+XA][y]=12;
			if (creatures[x+XA][y]!=CLEAR)
				CngScr(creatures[x+XA][y], 1);
			creatures[x+XA][y]=CLEAR;
			level[x+XA][y]=CLEAR;
			explode[x+XA][y]=BOOM;
			exptim[x+XA][y]=6;
		}
	}
	for (YA=-1; YA<=2; YA+=2) {
		if ((level[x][y+YA]==CLEAR)||
			  (level[x][y+YA]==WALL2)||
			  (level[x][y+YA]==WALL4)) {
			if (bomtim[x][y+YA]==50)
				bomtim[x][y+YA]=12;
			if (creatures[x][y+YA]!=CLEAR)
				CngScr(creatures[x][y+YA], 1);
			creatures[x][y+YA]=CLEAR;
			level[x][y+YA]=CLEAR;
			explode[x][y+YA]=BOOM;
			exptim[x][y+YA]=6;
		}
	}
}

void Bomb2Explode(int x,int y) {
	PlayBoomSnd();
	int xa;
	int ya;
	xa=x;
	ya=y;
	for (xa=x; xa<=14; xa++) {
		if (level[xa][ya]==CLEAR) {
			if (bomtim[xa][ya]==50)
				bomtim[xa][ya]=12;
			if (creatures[xa][ya]!=CLEAR)
				CngScr(creatures[xa][ya], 2);
			creatures[xa][ya]=CLEAR;
			explode[xa][ya]=BOOM;
			exptim[xa][ya]=6;
		}
		else if ((level[xa][ya]==WALL2)||
        (level[xa][ya]==WALL4)||
        (level[xa][ya]==WALL5)||
        (creatures[xa][ya]==TVAR4)) {
			if (bomtim[xa][ya]==50)
				bomtim[xa][ya]=12;
			if (creatures[xa][ya]!=CLEAR)
				CngScr(creatures[xa][ya], 2);
			creatures[xa][ya]=CLEAR;
			level[xa][ya]=CLEAR;
			explode[xa][ya]=BOOM;
			exptim[xa][ya]=6;
			xa=15;
		}
		else if ((level[xa][ya]==WALL3)||(level[xa][ya]==WALL))
			xa=15;
	}
	for (xa=x; xa>=0; xa--) {
		if (level[xa][ya]==CLEAR) {
			if (bomtim[xa][ya]==50)
				bomtim[xa][ya]=12;
			if (creatures[xa][ya]!=CLEAR)
				CngScr(creatures[xa][ya], 2);
			creatures[xa][ya]=CLEAR;
			explode[xa][ya]=BOOM;
			exptim[xa][ya]=6;
		}
		else if ((level[xa][ya]==WALL2)||
			(level[xa][ya]==WALL4)||
			(level[xa][ya]==WALL5)||
			(creatures[xa][ya]==TVAR4)) {
			if (bomtim[xa][ya]==50)
				bomtim[xa][ya]=12;
			if (creatures[xa][ya]!=CLEAR)
				CngScr(creatures[xa][ya], 2);
			creatures[xa][ya]=CLEAR;
			level[xa][ya]=CLEAR;
			explode[xa][ya]=BOOM;
			exptim[xa][ya]=6;
			xa=-1;
		}
		else if ((level[xa][ya]==WALL3)||(level[xa][ya]==WALL))
			xa=-1;
	}
	xa=x;
	for (ya=y; ya<=13; ya++) {
		if (level[xa][ya]==CLEAR) {
			if (bomtim[xa][ya]==50)
				bomtim[xa][ya]=12;
			if (creatures[xa][ya]!=CLEAR)
				CngScr(creatures[xa][ya], 2);
			creatures[xa][ya]=CLEAR;
			explode[xa][ya]=BOOM;
			exptim[xa][ya]=6;
		}
		else if ((level[xa][ya]==WALL2)||
			(level[xa][ya]==WALL4)||
			(level[xa][ya]==WALL5)||
			(creatures[xa][ya]==TVAR4)) {
			if (bomtim[xa][ya]==50)
				bomtim[xa][ya]=12;
			if (creatures[xa][ya]!=CLEAR)
				CngScr(creatures[xa][ya], 2);
			creatures[xa][ya]=CLEAR;
			level[xa][ya]=CLEAR;
			explode[xa][ya]=BOOM;
			exptim[xa][ya]=6;
			ya=15;
		}
		else if ((level[xa][ya]==WALL3)||(level[xa][ya]==WALL))
			ya=15;
	}
	for (ya=y; ya>=0; ya--) {
		if (level[xa][ya]==CLEAR) {
			if (bomtim[xa][ya]==50)
				bomtim[xa][ya]=12;
			if (creatures[xa][ya]!=CLEAR)
				CngScr(creatures[xa][ya], 2);
			creatures[xa][ya]=CLEAR;
			explode[xa][ya]=BOOM;
			exptim[xa][ya]=6;
		}
		else if ((level[xa][ya]==WALL2)||
			(level[xa][ya]==WALL4)||
			(level[xa][ya]==WALL5)||
			(creatures[xa][ya]==TVAR4)) {
			if (bomtim[xa][ya]==50)
				bomtim[xa][ya]=12;
			if (creatures[xa][ya]!=CLEAR)
				CngScr(creatures[xa][ya], 2);
			creatures[xa][ya]=CLEAR;
			level[xa][ya]=CLEAR;
			explode[xa][ya]=BOOM;
			exptim[xa][ya]=6;
			ya=-1;
		}
		else if ((level[xa][ya]==WALL3)||(level[xa][ya]==WALL))
			ya=-1;
	}
}

void Bomb3Explode(int x,int y) {
	PlayBoomSnd();
	int XA;
	int YA;
	for (XA=-1; XA<2; XA++) {
		if ((level[x+XA][y]==CLEAR)||
			(level[x+XA][y]==WALL2)||
			(level[x+XA][y]==WALL4)||
			(level[x+XA][y]==WALL5)) {
			if (bomtim[x+XA][y]==50)
				bomtim[x+XA][y]=12;
			if (creatures[x+XA][y]!=CLEAR)
				CngScr(creatures[x+XA][y], 3);
			creatures[x+XA][y]=CLEAR;
			level[x+XA][y]=CLEAR;
			explode[x+XA][y]=BOOM;
			exptim[x+XA][y]=6;
		}
	}
	for (YA=-1; YA<=2; YA+=2) {
		if ((level[x][y+YA]==CLEAR)
        || (level[x][y+YA]==WALL2)
        || (level[x][y+YA]==WALL4)
        || (level[x][y+YA]==WALL5)) {
			if (bomtim[x][y+YA]==50)
				bomtim[x][y+YA]=12;
			if (creatures[x][y+YA]!=CLEAR)
				CngScr(creatures[x][y+YA], 3);
			creatures[x][y+YA]=CLEAR;
			level[x][y+YA]=CLEAR;
			explode[x][y+YA]=BOOM;
			exptim[x][y+YA]=6;
		}
	}
}

void BlitPanell() {
	char s[10];
	// TBAPYC
  if (!tvarusPressed) {
		BlitImage(Vec2Si32(491, 0), panellSur, Vec4Si32(491, 0, 640, 49));
  } else {
		BlitImage(Vec2Si32(492, 1), panellSur, Vec4Si32(491, 0, 628, 43));
		BlitImage(Vec2Si32(491, 43), panellSur, Vec4Si32(491, 43, 640, 49));
	}
	// LIFES
	BlitImage(Vec2Si32(500, 80), panellSur, Vec4Si32(567, 420, 630, 432));
	// lifes
	sprintf(s, "%d", lifes);
	DisplayNumber(s, 570, 80);
	// SCORE
	BlitImage(Vec2Si32(512, 100), panellSur, Vec4Si32(515, 420, 566, 432));
	// score
	sprintf(s, "%d", score);
	DisplayNumber(s, 570, 100);
	// LEVEL
	BlitImage(Vec2Si32(501,120), panellSur, Vec4Si32(499, 312, 561, 324));
	// level
	sprintf(s, "%d", levelNum);
	DisplayNumber(s, 570, 120);


	// BOMB2 -
	BlitImage(Vec2Si32(535, 145), panellSur, Vec4Si32(0, 40, 40, 80));
	// bomb2num
	sprintf(s, "%d", bomb2num);
	DisplayNumber(s, 570, 145);

	// BOMB3 -
	BlitImage(Vec2Si32(535, 180), panellSur, Vec4Si32(160, 240, 200, 280));
	// bomb3num
	sprintf(s, "%d", bomb3num);
	DisplayNumber(s, 570, 180);


	// EXIT BUTTON
	if (exitPressed)
		BlitImage(Vec2Si32(500, 432), panellSur, Vec4Si32(565, 457, 630, 480));
	else
		BlitImage(Vec2Si32(500, 432), panellSur, Vec4Si32(565, 433, 630, 456));
	
	// NEW BUTTON
	if (newPressed)
		BlitImage(Vec2Si32(500, 378), panellSur, Vec4Si32(495, 349, 561, 372));
	else
		BlitImage(Vec2Si32(500, 378), panellSur, Vec4Si32(495, 325, 561, 348));
	
	// PAUSE BUTTON
	if (pausePressed)
		BlitImage(Vec2Si32(500, 405),	panellSur, Vec4Si32(498, 457, 564, 480));
	else
		BlitImage(Vec2Si32(500, 405),	panellSur, Vec4Si32(498, 433, 564, 456));

	// RESTART BUTTON
	if (restartPressed)
		BlitImage(Vec2Si32(570, 378), panellSur, Vec4Si32(215, 324, 274, 401));
	else
		BlitImage(Vec2Si32(570, 378), panellSur, Vec4Si32(215, 402, 274, 479));


	// PAUSE
	if (pauseMode) {
		BlitImage(Vec2Si32(200, 200), panellSur, Vec4Si32(509, 373, 630, 419));
		BlitImage(Vec2Si32(500, 300), panellSur, Vec4Si32(564, 73, 640, 141));
	}

	// END!
	if (gameOver)
		BlitImage(Vec2Si32(200, 150), panellSur, Vec4Si32(516, 142, 640, 188));

	// CURSOR CALCULATIONS
	int xchg=oldMousex-mousex;
	int ychg=oldMousey-mousey;

	if ((xchg==0)&&(ychg>0))
		mouseDirection=0;
	else if ((xchg==0)&&(ychg<0))
		mouseDirection=1;
	else if ((xchg>0)&&(ychg==0))
		mouseDirection=2;
	else if ((xchg<0)&&(ychg==0))
		mouseDirection=3;
	else if ((xchg>0)&&(ychg>0))
		mouseDirection=4;
	else if ((xchg<0)&&(ychg>0))
		mouseDirection=5;
	else if ((xchg>0)&&(ychg<0))
		mouseDirection=6;
	else if ((xchg<0)&&(ychg<0))
		mouseDirection=7;

	Ui32 lux=562+(23*mouseCader);
	Ui32 rdx=lux+22;
	Ui32 luy=189+(23*mouseDirection);
	Ui32 rdy=luy+22;

	// BLIT MOUSE CURSOR
	BlitImage(Vec2Si32(mousex-11, mousey-11), panellSur, Vec4Si32(lux, luy, rdx, rdy));
}

void OnMouseMove(int x, int y) {
	if (exitAfterKey)
		return;
	oldMousex=mousex;
	oldMousey=mousey;
	mousex = x;
	mousey = y;
	AffectMouse();
	return;
}

void OnLButtonDown(int x, int y) {
  if (exitAfterKey) {
    doExit = true;
  }
	// sound: click !
  clickSnd.Play();

	if (pauseMode)
		return;
	lButtonDown = true;
	AffectMouse();
	return;
}

void OnLButtonUp(int x, int y) {
  if (exitAfterKey) {
    doExit = true;
  }

	// sound: unclick !
  unclickSnd.Play();

	lButtonDown = false;
	if (pauseMode) {
		if ((mousex>500)&&(mousex<576)&&(mousey>300)&&(mousey<368)) {
			UnPause();
			AffectMouse();
		}
		return;
	}
	if (coolOnline) {
		if ((mousex>564)&&(mousex<640)&&(mousey>412)&&(mousey<480)) {
			UnCool();
			AffectMouse();
		}
		return;
	}
	if (tvarusPressed)
		CoolStuff();
  if (exitPressed) {
    doExit = true;
  }
	if (pausePressed)
		Pause();
	if (restartPressed) {
		if (score<10)
			score=100;
		Restart();
	}
	if (newPressed)
		NewGame();
	AffectMouse();
	return;
}

void AffectMouse() {
	if (coolOnline) {
    Clear();
		PaintCool();
		return;
	}
	tvarusPressed = false;
	pausePressed = false;
	restartPressed = false;
	exitPressed = false;
	newPressed = false;
	if ((mousex>502)&&(mousex<628)&&(mousey>8)&&(mousey<42)&&(lButtonDown))
		tvarusPressed = true;
	if ((mousex>570)&&(mousex<629)&&(mousey>378)&&(mousey<455)&&(lButtonDown))
		restartPressed = true;
  Clear();
	BlitImage(Vec2Si32(570, 378),
		panellSur, Vec4Si32(215, 402, 274, 479));

	if ((mousex>500)&&(mousex<566)) {
		// mouse is in buttons zone
		if ((mousey>405)&&(mousey<429)) {
			// mouse is on 'Pause'
			if (lButtonDown)
				pausePressed = true;
		}
		if ((mousey>432)&&(mousey<456)) {
			// mouse is on 'Exit'
			if (lButtonDown)
				exitPressed = true;
		}
		if ((mousey>378)&&(mousey<402)) {
			// mouse is on 'New'
			if (lButtonDown)
				newPressed = true;
		}
	}
	PaintScreen();
}

//////////////////////////////////////////////////////////////////
//Pause()
//
//////////////////////////////////////////////////////////////////
void Pause() {
	pauseMode = true;
  Clear();
	PaintScreen();
}

//////////////////////////////////////////////////////////////////
//UnPause()
//
//////////////////////////////////////////////////////////////////
void UnPause() {
	if (gameOver)
		NewGame();
	gameOver = false;
	if (restart) {
		LoadLevel(levelNum);
		restart = false;
	}
	pauseMode = false;
}

//////////////////////////////////////////////////////////////////
//OnTimer()
//
//////////////////////////////////////////////////////////////////
void OnTimer() {
	mouseCader++;
	if (mouseCader > 2)
		mouseCader = 0;
	if (coolOnline == true) {
    Clear();
		PaintCool();
		return;
	}
	tick++;
	if (tick > 2)
		tick = 1;
	mantick++;
	if (mantick > 3)
		mantick = 0;
	t1SholdMove = false;
	if (tick == 1)
		t1SholdMove = true;

	WallRedraw();
	CBomTim();
	char ch = creatures[manx][many];
	if (ch == TVAR1) {
    death1Snd.Play();
		Restart();
	}	else if (ch == TVAR2) {
		death1Snd.Play();
		Restart();
	}	else if (ch == TVAR3) {
		death1Snd.Play();
		Restart();
	}	else if (ch == TVAR5) {
		death1Snd.Play();
		Restart();
	}	else if (ch == TVAR6) {
		death1Snd.Play();
		Restart();
	}
	if ((explode[manx][many] == BOOM)&&(exptim[manx][many] == 5))
		Restart();
	CheckOnDone();
}

void DisplayNumber(char* s, Ui32 x, Ui32 y) {
	Ui32 len = (Ui32)strlen(s);

	// start cycling
	for (Ui32 i = 0; i < len; ++i) {
		// make char into int
		char ch = s[i];
		Ui32 digit = atoi(&ch);

		// blit number
		BlitImage(Vec2Si32(x+i*10,y), panellSur, Vec4Si32(631,341+digit*14,640,354+digit*14));
	}
}

void NewGame() {
	score = 100;
	lifes = 3;
	levelNum = 1;
	LoadLevel(levelNum);
}

void GameOver() {
	gameOver = true;
	Pause();
}

void Restart() {
	bomb2num = 0;
	bomb3num = 0;
	curputbomb3 = 0;
	expbomb3 = false;
	lifes--;
	if (lifes < 0) {
		lifes++;
		GameOver();
	} else {
		restart = true;
		Pause();
	}
	return;
}

void WallRedraw() {
	int x = 0;
	int y = 0;
	for (x = 0; x < 15; x++)
		for (y = 0; y < 14; y++)
		wallredraw[x][y] = rand() % 3;
}

void CheckOnDone() {
	int x;
	int y;
	bool done = true;
	for (x = 0; x < 15; x++)
		for (y = 0; y < 14; y++) {
		if (creatures[x][y] != CLEAR) {
			done = false;
			x=15;
			y=14;
		}
	}
	if (!done)
		return;
	levelNum++;
	bomb2num=0;
	bomb3num=0;
	LoadLevel(levelNum);
	//ShowDone();
	if (levelNum != 14)
		Pause();
}

void GameComplete() {
	if (score < 100) {
		exitAfterKey = true;
		oblomis = true;
		return;
	}
  
  if (!isLevelEditorUnlocked) {
    UnlockLevelEditor();
  }

	exitAfterKey = true;
}

void CoolStuff() {
	Cool();
}

void Cool() {
	coolOnline = true;
	// Kill game timer;
	//KillTimer(hWindow, 1);

  Clear();
	PaintCool();
}

void UnCool() {
	coolOnline = false;
  if (isLevelEditorUnlocked) {
    LevelEditor();
  }
}

void PaintCool() {
	// COOL Background
	BlitImage(Vec2Si32(0, 0),
		coolSur, Vec4Si32(0, 0, 640, 480));

	// PUSH HERE
	BlitImage(Vec2Si32(564, 412),
		panellSur, Vec4Si32(564, 73, 640, 141));

	// CURSOR CALCULATIONS
	int xchg=oldMousex-mousex;
	int ychg=oldMousey-mousey;

	if ((xchg==0)&&(ychg>0))
		mouseDirection=0;
	else if ((xchg==0)&&(ychg<0))
		mouseDirection=1;
	else if ((xchg>0)&&(ychg==0))
		mouseDirection=2;
	else if ((xchg<0)&&(ychg==0))
		mouseDirection=3;
	else if ((xchg>0)&&(ychg>0))
		mouseDirection=4;
	else if ((xchg<0)&&(ychg>0))
		mouseDirection=5;
	else if ((xchg>0)&&(ychg<0))
		mouseDirection=6;
	else if ((xchg<0)&&(ychg<0))
		mouseDirection=7;

	Ui32 lux=562+(23*mouseCader);
	Ui32 rdx=lux+22;
	Ui32 luy=189+(23*mouseDirection);
	Ui32 rdy=luy+22;

	// BLIT MOUSE CURSOR
	BlitImage(Vec2Si32(mousex-11, mousey-11),
		panellSur, Vec4Si32(lux, luy, rdx, rdy));
}

void SuperTimer() {
	static int x=100;
	static int y=0;
	static int ha=1;
	static int va=1;

	static int x2=200;
	static int y2=100;
	static int ha2=-1;
	static int va2=-1;

	if (!oblomis) {
		if (x > 434) {
			bounceSnd.Play();
			ha = -1;
		}
		if (x < 1) {
			bounceSnd.Play();
			ha = 1;
		}
		if (y > 443) {
			bounceSnd.Play();
			va = -1;
		}
		if (y < 1) {
			bounceSnd.Play();
			va = 1;
		}

		if (x2 > 420) {
			bounceSnd.Play();
			ha2 = -1;
		}
		if (x2 < 1) {
			bounceSnd.Play();
			ha2 = 1;
		}
		if (y2 > 369) {
			bounceSnd.Play();
			va2 = -1;
		}
		if (y2 < 1) {
			bounceSnd.Play();
			va2 = 1;
		}
		y += va;
		x += ha;

		y2 += va2;
		x2 += ha2;

		BlitImage(Vec2Si32(x, y), panellSur, Vec4Si32(300, 333, 494, 369));
		BlitImage(Vec2Si32(x2, y2), panellSur, Vec4Si32(275, 370, 494, 480));
	} else {
		if (x > 475) {
			tuhSnd.Play();
			ha = -1;
		}
		if (x < 1) {
			tuhSnd.Play();
			ha = 1;
		}
		if (y > 435) {
			tuhSnd.Play();
			va = -1;
		}
		if (y < 1) {
			tuhSnd.Play();
			va = 1;
		}

		if (x2 > 444) {
			tuhSnd.Play();
			ha2 = -1;
		}
		if (x2 < 1) {
			tuhSnd.Play();
			ha2 = 1;
		}
		if (y2 > 444) {
			tuhSnd.Play();
			va2 = -1;
		}
		if (y2 < 1) {
			tuhSnd.Play();
			va2 = 1;
		}
		y += va;
		x += ha;

		y2 += va2;
		x2 += ha2;

		BlitImage(Vec2Si32(x, y), panellSur, Vec4Si32(351, 117, 515, 161));
		BlitImage(Vec2Si32(x2, y2), panellSur, Vec4Si32(368, 81, 563, 116));
	}
}

void CngScr(char cr, int x) {
	float p=(float)0;
	float p2=(float)0;
	float adj;
	if (cr==TVAR1)
		p=(float)1;
	else if (cr==TVAR2)
		p=(float)2;
	else if (cr==TVAR3)
		p=(float)3;
	else if (cr==TVAR4)
		p=(float)0;
	else if (cr==TVAR5)
		p=(float)4;
	else if (cr==TVAR6)
		p=(float)5;

	if (x==1)
		p2=(float)3;
	else if (x==2)
		p2=(float)1;
	else if (x==3)
		p2=(float)2;

	adj=p*p2;
	score+=int(adj);
}

bool InitSound() {
	clickSnd.Load("data/click.wav");
	unclickSnd.Load("data/unclick.wav");
	bounceSnd.Load("data/clunc.wav");
	tuhSnd.Load("data/tuh.wav");
	boom1Snd.Load("data/boom1.wav");
	boom2Snd.Load("data/boom2.wav");
	boom3Snd.Load("data/boom3.wav");
	death1Snd.Load("data/death1.wav");
	return true;
}

void PlayBoomSnd() {
	int xm = (rand() % 3);
	if (xm == 1)
    boom1Snd.Play();
	else if (xm == 2)
    boom2Snd.Play();
	else boom3Snd.Play();
}
